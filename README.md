# Management

This project is meant to track my personal journey to the management path

## Issues

I use [issues](https://gitlab.com/abrams-career-development/management/-/issues) to capture various projects, goals, and progress.

Original manager onboarding issue: https://gitlab.com/gitlab-com/people-group/Training/-/issues/2508

### Transitioning from an individual contributor to manager

The [GitLab handbook page on transitioning from an indivicual contributor to manager](https://about.gitlab.com/handbook/engineering/development/dev/training/ic-to-manager/) has many helpful tasks and advice for this journey.

A special thanks to @mksionek for sharing a set of issues inspired by this page to track progress through this content! Also, thank you for introducing me to the SMART framework for setting goals!

Issues with the ~"Transitioning from IC to manager" label are related to this content.
